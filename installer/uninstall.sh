echo "... removing from /etc/rc.local"
TARGET="/etc/rc.local"
API="node /home/pi/urbanspecies/api/api.js &"
UI="serve /home/pi/urbanspecies/build -s -p 8080 &"

sudo sed -i "s#$API##" $TARGET
sudo sed -i "s#$UI##" $TARGET

echo " ... done"
