# Software installation
> Steps to follow for a new SD copy of the OS

## Connect the RPI to a wireless network

- After a fresh installation of any Rapbian OS on an SD, **enable SSH access** by adding and empty `ssh` file on `/boot/` the code bellow can make the trick
  
```code
touch ssh
```
-  Create a `wpa_supplicant.conf` file with the wireless credentials, and save on `/boot/` the code bellow can be used as a template

```code
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
country=PE
update_config=1

network={
    ssid="mywifi"
    psk="mysecretpass"
}

```
## Run the setup.sh

Now, connect to the RPi with your preferred ssh client and run the installer (You must have internet connection to download all required dependencies)
```bash
sh setup.sh
```
After the setup has finished reboot the RPI
```BASH
sudo reboot now
```
Now, the UI should be available on 
```code
http://raspberryip:8080
```

## Uninstall

Run the uninstall script, it will remove the admin from the startup but it will keep the dependencies.
```BASH
sh uninstall.sh
```
