echo " ... update"
sudo apt-get update
echo " ... install git"
sudo apt-get install git -y

echo " ... download nodejs"
curl -sSL https://deb.nodesource.com/setup_16.x | sudo bash -
echo " ... install nodejs"
sudo apt install -y nodejs

echo " ... install service manager"
sudo npm i -g pm2
sudo npm i -g serve

echo " ... clone repo and install dependencies"
cd /home/pi
git clone https://gitlab.com/km115.franco/urbanspecies.git
cd /home/pi/urbanspecies/api
npm i

echo "... add to /etc/rc.local"
TARGET="/etc/rc.local"
API="node /home/pi/urbanspecies/api/api.js &"
UI="serve /home/pi/urbanspecies/build -s -p 8080 &"

if grep -Fq "$API" $TARGET
then
echo " ... backend already installed"
else
echo " ... installing backend"
sudo sed -i "/^exit 0.*/i $API" $TARGET
fi

if grep -Fq "$UI" $TARGET
then
echo " ... frontend already installed"
else
echo " ... installing frontend"
sudo sed -i "/^exit 0.*/i $UI" $TARGET
fi

echo " ... done"

