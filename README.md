# urbanspecies

> Tool to manage urbanspecies toolkit recipes

- [urbanspecies](#urbanspecies)
  - [Development instructions](#development-instructions)
  - [Install instructions](#install-instructions)
  - [Folder structure](#folder-structure)
  - [Recipe data](#recipe-data)
  - [Config UI](#config-ui)
  - [Content types](#content-types)
    - [Select type](#select-type)
      - [User options](#user-options)
      - [Options from media files](#options-from-media-files)
    - [Multiple-select type](#multiple-select-type)
    - [Array type](#array-type)
      - [Array type set of texts](#array-type-set-of-texts)
      - [Array type set of multiple-select](#array-type-set-of-multiple-select)
  - [Save configuration](#save-configuration)
  - [Retrieve fields](#retrieve-fields)

## Development instructions

* Clone the repository
* Run `npm i` to install the frontend dependencies
* Run `npm start` to run the UI in development mode
* Go to `api` with `cd api`
* Run `npm i` to install the backend dependencies
* Run the backend with `nodemon api.js`

## Install instructions
Go to the [install](./installer) directory of this repo

## Folder structure

To be accuratelly managed with this UI the recipe must contain the `media/` folder  `recipe-data.json` and `recipe-config.json`
Like the example bellow:

```BASH
example_recipe/
├── script.py
├── helpers.py
├── media
│   ├── red.mp3
│   ├── white.mp3
│   └── yellow.mp3
├── recipe-config.json
└── recipe-data.json
```
## Recipe data

In order to control and display the recipe process, the `recipe-data.json` should contain basic information, as a reference use the template bellow:

```JS
{
  "name": "Demo Example",
  "description": "Script to test",
  "version": "0.1.1",
  "script": "script.py",
  "interpreter": "python3"
}
```
`name` is the friendly name of the recipe, `script` is the peace of code to be launched by the process manager, the other keys names are self explanatory.


## Config UI

One of the features of this project is that it can render options and input boxes based on a json configuration file
The target config file is: `recipe-config.json` and it should have the following structure

```JS
[
    {
        "property": "message",
        "label": "Text to show",
        "value": "Aloha",
        "placeholder": "Type here",
        "type": "text",
        "validators": {
            "maxLength": 15,
            "minLength": 1
        }
    },
    {
        "property": "period",
        "label": "Period to display",
        "value": 10,
        "placeholder": "Type here",
        "type": "int",
        "validators": {
            "maxLength": 15,
            "minLength": 1,
            "min": 1,
            "max": 10
        }
    }
]
```

Where `property` is the name of the variable we are going to asociate the `value` , `label` is the friendly name of the variable and will be displayed on the form. Then, `type` is the type of element we are going to render, for instance, if `"type" = "float"` the UI will display an input box that only admits float numbers. In addition to this, there is the key `validations` that contains HTML validations that we can use to constrain the input values.

The json example described before, will render the following:

<div align="center"> 
  <img src="./imgs/readme1.png" />
</div>

## Content types

At this point of the development the available types are:
Type|Description
-----|-----
text|Text input
int|Number input
float|Number input
select|Displays a dropdown list with an item to select
multiple-select|Displays a set of items to select by clicking
array|Displays a subset of any of the types described before

### Select type

This type contains an `options` key where we should have an array of the options to choose from. Furthermore, if we want the user to choose from the media available we need to add the following: `"source": "media"` .

#### User options

```JS
{
    "property": "color",
    "label": "Color display",
    "value": "red",
    "type": "select",
    "options": [
        "blue",
        "red",
        "yellow"
    ]
},
```

<div align="center">
  <img src="./imgs/readme2.png">
</div>

#### Options from media files

```JS
{
    "property": "color",
    "label": "Color display",
    "value": "red",
    "type": "select",
    "options": [],
    "source": "media"
},
```

<div align="center">
  <img src="./imgs/readme3.png">
</div>

### Multiple-select type

Similar to the `select` type. This type allow to have multiple items selected or none, therefore the `value` is an array. The `source` key can be applied as well if we want to populate the files from the recipe media

```JS
{
    "property": "colors",
    "label": "Color selections",
    "value": ["blue"],
    "type": "multiple-select",
    "options": ["blue", "red", "yellow", "brown"]
}, 
```
<div align="center">
  <img src="./imgs/readme4.gif">
</div>

### Array type

As the name suggest, this type allows to save a set of elements therefore we should especify the features of that `element`, the element can be a `number`, `string`, `select` or `multiple select`. In addition there are array controls like `add` and `remove`. One little drawback of the array type is that the user **must** save locally, clicking the save button, as the example bellow:

#### Array type set of texts

```JS
{
  "property": "countries",
  "label": "Set of countries",
  "value": [],
  "type": "array",
  "element": {
    "label": "Country",
    "value": "",
    "placeholder": "Type a country",
    "type": "text",
    "validators": {
      "maxLength": 15,
      "minLength": 1
    }
  }
},
```

  <div align="center">
    <img src="./imgs/readme5.gif">
  </div>

#### Array type set of multiple-select

```JS
{
    "property": "sounds",
    "label": "Set of sounds",
    "value": [
        [
            "red.mp3",
            "white.mp3"
        ],
        [
            "yellow.mp3",
            "white.mp3"
        ]
    ],
    "type": "array",
    "element": {
        "label": "Sounds",
        "value": [],
        "type": "multiple-select",
        "options": [],
        "source": "media"
    }
},
```

<div align="center">
  <img src="./imgs/readme6.gif">
</div>

## Save configuration

After editing the fields the user **must** press the save button to update the `recipe-config.json` file. The save button is at the end of the configuration tab.

<div align="center"> 
  <img src="./imgs/readme7.gif" />
</div>

## Retrieve fields

As noted every object has a `property`, the property is the name of the variable we want to use on the recipe script, then the variable `value` is what we need to get from our python code. Please notice that the property name should follow the python naming conventions.

The code bellow can be used to read the configuration file and get the value for the selected property.

```python
# helpers.py
from os import path
from json import load


def get_config():
    configPath = path.dirname(path.realpath(__file__))
    configFile = 'recipe-config.json'
    return load(open("%s/%s" % (configPath, configFile)))


def get_value(config, key):
    prop = list(filter(
        lambda x: x["property"] == key, config
    ))[0]
    return prop["value"]

```

Then, in the recipe script we can use the following code to get the values:

```python
# recipe_script.py
from helpers import get_value, get_config

config = get_config()
sounds = get_value(config, "sounds")
colors = get_value(config, "colors")
...
```
