"use strict";
const fs = require("fs");
const os = require("os");
const sys = require("child_process");
const Redis = require("ioredis");
const redis = new Redis();

const remoteRepo = "https://gitlab.com/km115.franco/urbanspecies.git";

const development = os.arch() !== "arm";
const absPath = development ? "/home/fx/Upwork/" : "/home/pi/";
const folder = absPath + "toolkit/recipes";
const autostartFile =
  absPath + "toolkit/software/recipe_manager/urbanspecies/api/autostart.json";

const infoJSON = "recipe-data.json";
const configJSON = "recipe-config.json";

// prompt working environment
if (development) console.log("... development");
else console.log("... production (rpi)");

// Create processes from files
const getRecipes = () => {
  const recipes = [];
  const folderSettings = [
    {
      name: "community",
      settings: {
        daemon: false,
        core: false,
        example: true,
      },
    },
    {
      name: "core",
      settings: {
        daemon: true,
        core: true,
        example: false,
      },
    },
    {
      name: "examples",
      settings: {
        daemon: false,
        core: false,
        example: true,
      },
    },
    {
      name: "myRecipes",
      settings: {
        daemon: false,
        core: false,
        example: false,
      },
    },
  ];
  const recipeDefaults = {
    running: false,
    logs: false,
    configure: false,
    media: false,
    info: false,
  };
  const classified = folderSettings.map((f) => f.name);

  fs.readdirSync(folder).forEach((type) => {
    if (classified.includes(type)) {
      fs.readdirSync(`${folder}/${type}`).forEach((recipe) => {
        let stats = fs.statSync(`${folder}/${type}/${recipe}`);
        if (!stats.isFile()) {
          const content = [];
          fs.readdirSync(`${folder}/${type}/${recipe}`).forEach((f) =>
            content.push(f)
          );
          if (content.includes(infoJSON)) {
            const recipeType = folderSettings.find((f) => f.name == type);
            const recipeInfo = JSON.parse(
              fs.readFileSync(`${folder}/${type}/${recipe}/${infoJSON}`)
            );
            const recipeModel = {
              ...recipeType.settings,
              ...recipeInfo,
              ...recipeDefaults,
              path: `${folder}/${type}/${recipe}`,
            };

            recipes.push(recipeModel);
          }
        }
      });
    }
  });
  return recipes;
};

// read config from file
const getConfig = (path) => {
  try {
    return fs.readFileSync(`${path}/${configJSON}`);
  } catch (error) {
    if (error.message.includes("no such file or directory"))
      console.log("no config file found");
  }
  return [];
};

const getInfo = (path) => {
  try {
    return fs.readFileSync(`${path}/${infoJSON}`);
  } catch (error) {
    if (error.message.includes("no such file or directory"))
      console.log("no config file found");
  }
  return [];
};

const getDaemons = () => {
  try {
    const recipes = JSON.parse(fs.readFileSync(`${autostartFile}`));
    recipes.forEach((element) => {
      console.log(element.name);
    });

    return recipes;
  } catch (error) {
    if (error.message.includes("no such file or directory"))
      console.log("... no autostart file found");
  }
  return [];
};

const setDaemons = (recipes) => {
  console.log("... set daemons");
  try {
    fs.writeFileSync(`${autostartFile}`, JSON.stringify(recipes, null, 4));
    return true;
  } catch (err) {
    console.log(err);
    return false;
  }
};

const setConfig = (path, properties) => {
  try {
    fs.writeFileSync(
      `${path}/${configJSON}`,
      JSON.stringify(properties, null, 4)
    );
    return true;
  } catch (err) {
    console.log(err);
    return false;
  }
};

const setInfo = (path, properties) => {
  try {
    fs.writeFileSync(
      `${path}/${infoJSON}`,
      JSON.stringify(properties, null, 4)
    );
    return true;
  } catch (err) {
    console.log(err);
    return false;
  }
};

const getMedia = () => {
  const media = [];
  const mediaFolder = "media";
  try {
    fs.readdirSync(`${folder}/${mediaFolder}`).forEach((file) => {
      media.push(file);
    });
  } catch (err) {
    console.log(err);
  }

  return media;
};

const removeFile = (file) => {
  const mediaFolder = "media";
  try {
    fs.unlinkSync(`${folder}/${mediaFolder}/${file}`);
    return true;
  } catch (error) {
    return false;
  }
};

const getProcessMedia = (path) => {
  const media = [];
  const mediaFolder = "media";
  try {
    fs.readdirSync(`${path}/${mediaFolder}`).forEach((file) => {
      media.push(file);
    });
  } catch (error) {
    if (error.message.includes("no such file or directory"))
      console.log("no media directory found");
  }
  return media;
};

const removeProcessFile = (target) => {
  const { path, file } = target;
  console.log(path);
  console.log(file);
  const mediaFolder = "media";
  try {
    fs.unlinkSync(`${path}/${mediaFolder}/${file}`);
    return true;
  } catch (error) {
    return false;
  }
};

const createPath = (path) => {
  fs.existsSync(path) || fs.mkdirSync(path);
};

const shutdownHost = () => {
  if (development) console.log("... turning off");
  else sys.exec("sleep 1 && sudo shutdown now");
};

const restartHost = () => {
  if (development) console.log("... rebooting");
  else sys.exec("sleep 1 && sudo reboot now");
};

const getCurrentVersion = () => {
  return sys.execSync("git describe --abbrev=0").toString();
};

const getLatestVersion = () => {
  return sys
    .execSync(
      `git ls-remote --refs --sort='version:refname' --tags ${remoteRepo} | tail --lines=1 | cut --delimiter='/' --fields=3`
    )
    .toString();
};

const updateFromRepository = () => {
  sys.execSync(
    `cd ${absPath}toolkit && sudo git reset --hard && sudo git pull origin script-info`
  );
  sys.execSync(
    `cd ${absPath}toolkit && sudo git submodule update --recursive --remote`
  );
  return true;
};

const copyToMyRecipes = (source) => {
  const destination = source.replace("/examples/", "/myRecipes/");
  const info = JSON.parse(getInfo(source));
  info.name = info.name.replace(/\*$/, "");
  sys.execSync(`cp -r ${source} ${destination}`);
  setInfo(destination, info);
  return true;
};

const removeFromMyRecipes = (path) => {
  sys.execSync(`rm -rf ${path}`);
  return true;
};

const sendToRedis = (payload) => {
  redis.publish(payload.channel, payload.payload);
};

exports.getRecipes = getRecipes;
exports.getConfig = getConfig;
exports.getDaemons = getDaemons;
exports.setDaemons = setDaemons;
exports.setConfig = setConfig;
exports.getMedia = getMedia;
exports.removeFile = removeFile;
exports.getProcessMedia = getProcessMedia;
exports.removeProcessFile = removeProcessFile;
exports.createPath = createPath;
exports.shutdownHost = shutdownHost;
exports.restartHost = restartHost;
exports.getLatestVersion = getLatestVersion;
exports.getCurrentVersion = getCurrentVersion;
exports.updateFromRepository = updateFromRepository;
exports.copyToMyRecipes = copyToMyRecipes;
exports.removeFromMyRecipes = removeFromMyRecipes;
exports.sendToRedis = sendToRedis;
exports.folder = folder;
