import axios from "axios";
import io from "socket.io-client";

export const host = `http://${window.location.hostname}:10000`;

export const socket = io.connect(`${host}`);

export const testHost = async () => {
  const url = `${host}`;
  const response = await axios.get(url);
  return response.data;
};

export const startProcess = async (process) => {
  console.log("... starting process");
  const url = `${host}/start`;
  const response = await axios.post(url, process);
  return response.data;
};

export const stopProcess = async (process) => {
  console.log("... stopping process");
  const url = `${host}/stop`;
  const response = await axios.post(url, process);
  return response.data;
};

export const restartProcess = async (process) => {
  console.log("... restarting process");
  const url = `${host}/restart`;
  const response = await axios.post(url, process);
  return response.data;
};

export const getLogs = async (process) => {
  console.log("... get logs");
  const url = `${host}/logs`;
  const response = await axios.post(url, process);
  return response.data;
};

export const listProcesses = async () => {
  console.log("... listing processes");
  const url = `${host}/ls`;
  const response = await axios.get(url);
  return response.data;
};

export const getRecipes = async () => {
  console.log("... get processes from files");
  const url = `${host}/recipes`;
  const response = await axios.get(url);
  return response.data;
};

export const getConfig = async (process) => {
  console.log("... get config");
  const url = `${host}/config/read`;
  const response = await axios.post(url, process);
  return response.data;
};

export const setConfig = async (process, properties) => {
  console.log("... set config");
  const { path } = process;
  const url = `${host}/config/write`;
  const response = await axios.post(url, { path, properties });
  return response.data;
};

export const getMedia = async () => {
  const url = `${host}/media`;
  const response = await axios.get(url);
  return response.data;
};

export const addMedia = async (payload) => {
  console.log("... uploading data");
  const url = `${host}/media/add`;
  const response = await axios.post(url, payload);
  return response.data;
};

export const removeMedia = async (file) => {
  console.log("... removing media");
  const url = `${host}/media/delete`;
  const response = await axios.post(url, file);
  return response.data;
};

export const getProcessMedia = async (process) => {
  const url = `${host}/media/read`;
  const response = await axios.post(url, process);
  return response.data;
};

export const removeProcessFile = async (payload) => {
  const url = `${host}/media/delete`;
  const response = await axios.post(url, payload);
  return response.data;
};

export const saveDaemons = async (recipes) => {
  const url = `${host}/daemons`;
  const response = await axios.post(url, recipes);
  return response.data;
};

export const getDaemons = async () => {
  const url = `${host}/daemons`;
  const response = await axios.get(url);
  return response.data;
};

export const shutdownHost = async () => {
  const url = `${host}/shutdown_host`;
  const response = await axios.get(url);
  return response.data;
};

export const restartHost = async () => {
  const url = `${host}/restart_host`;
  const response = await axios.get(url);
  return response.data;
};

export const getCurrentVersion = async () => {
  const url = `${host}/version/current`;
  const response = await axios.get(url);
  return response.data;
};

export const getLatestVersion = async () => {
  const url = `${host}/version/latest`;
  const response = await axios.get(url);
  return response.data;
};

export const updateFromRepository = async () => {
  const url = `${host}/version/update`;
  const response = await axios.get(url);
  return response.data;
};

export const getFile = async (path, file) => {
  const url = `${path}/${file}`;
  const response = await axios.get(url);
  return response.data;
};

export const copyToMyRecipes = async (recipe) => {
  const url = `${host}/my-recipe/copy`;
  const response = await axios.post(url, recipe);
  return response.data;
};

export const removeFromMyRecipes = async (recipe) => {
  const url = `${host}/my-recipe/remove`;
  const response = await axios.post(url, recipe);
  return response.data;
};

export const sendToRedis = async (payload) => {
  const url = `${host}/redis/send`;
  const response = await axios.post(url, payload);
  return response.data;
};
