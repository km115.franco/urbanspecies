export const toDateTime = (timestamp) => {
  const time = new Date(timestamp);
  return time.toLocaleString("sv-SE");
};

export const getIndexLabel = (settings, label, index) => {
  if (label.includes(" $")) {
    let keyword = label.match(/(\s\$\S+\b)/gi)[0];
    let key = keyword.replace(" $", "");
    const text =
      key !== "index"
        ? settings.find((x) => x.property === key).value[index]
        : (index + 1).toString();

    return label.replace(keyword, ` ${text || ""}`);
  }
  return label;
};
