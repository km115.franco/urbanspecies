import { action } from "easy-peasy";

const model = {
  processes: [],
  setProcesses: action((state, processes) => {
    state.processes = [...processes];
  }),
  addProcess: action((state, process) => {
    const oldProcesses = [...state.processes];
    oldProcesses.push(process);
    state.processes = oldProcesses;
  }),
  removeProcess: action((state, process) => {
    const oldProcesses = [...state.processes];
    state.processes = oldProcesses.filter((x) => x.name !== process.name);
  }),

  setDaemon: action((state, name) => {
    state.processes = state.processes.map((process) => {
      return process.name === name
        ? { ...process, daemon: !process.daemon }
        : { ...process };
    });
  }),
  setRunning: action((state, payload) => {
    const { name, running } = payload;
    state.processes = state.processes.map((process) => {
      return process.name === name ? { ...process, running } : process;
    });
  }),
  setLogs: action((state, name) => {
    state.processes = state.processes.map((process) => {
      return process.name === name
        ? { ...process, logs: !process.logs }
        : { ...process, logs: false };
    });
  }),
  setConfigure: action((state, name) => {
    state.processes = state.processes.map((process) => {
      return process.name === name
        ? { ...process, configure: !process.configure }
        : { ...process, configure: false };
    });
  }),
  setMedia: action((state, name) => {
    state.processes = state.processes.map((process) => {
      return process.name === name
        ? { ...process, media: !process.media }
        : { ...process, media: false };
    });
  }),
  setInfo: action((state, name) => {
    state.processes = state.processes.map((process) => {
      return process.name === name
        ? { ...process, info: !process.info }
        : { ...process, info: false };
    });
  }),
  setExample: action((state, payload) => {
    const { name, example } = payload;
    state.processes = state.processes.map((process) => {
      return process.name === name ? { ...process, example } : process;
    });
  }),

  recipeSettings: [],
  setRecipeSettings: action((state, recipeSettings) => {
    state.recipeSettings = recipeSettings;
  }),
};

export default model;
