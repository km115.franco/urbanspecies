const Firefly = () => {
  const host = window.location.hostname;
  const port = "8000";
  return (
    <div className="container is-fullwidth">
      <iframe title="Firefly debug" src={`http://${host}:${port}/`} />;
    </div>
  );
};

export default Firefly;
