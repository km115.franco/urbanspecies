import ActionRestart from "../actions/ActionRestart";
import ActionSetDaemon from "../actions/ActionSetDaemon";
import ActionSetExample from "../actions/ActionSetExample";
import ActionSetRun from "../actions/ActionSetRun";

const RecipeActions = ({ process }) => {
  return (
    <>
      {process.core ? (
        <>
          <ActionRestart process={process} />
        </>
      ) : (
        <>
          <ActionSetRun process={process} />
          {!process.example && <ActionSetDaemon process={process} />}
          <ActionSetExample process={process} />
        </>
      )}
    </>
  );
};

export default RecipeActions;
