import {
  faPauseCircle,
  faExclamationCircle,
  faCheckCircle,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const RecipeDetails = ({ process }) => {
  const { daemon, running, name, description } = process;
  return (
    <td className="is-flex is-flex-wrap-nowrap">
      <FontAwesomeIcon
        icon={
          daemon
            ? running
              ? faCheckCircle
              : faExclamationCircle
            : faPauseCircle
        }
        size="2x"
        className={
          daemon
            ? running
              ? "has-text-success mt-2"
              : "has-text-danger mt-2"
            : "has-text-grey mt-2"
        }
      />
      <div className="ml-4">
        <p className="subtitle is-5 mb-0 no-wrap">{name}</p>
        <p className="has-text-dark no-wrap">{description}</p>
      </div>
    </td>
  );
};

export default RecipeDetails;
