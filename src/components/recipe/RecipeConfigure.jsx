import { faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useStoreActions } from "easy-peasy";
import { useEffect, useState } from "react";
import { getConfig } from "../../js/api";
import JSONBasedConfig from "../JSONBasedConfig";

const RecipeConfigure = ({ process }) => {
  const setConfigure = useStoreActions((actions) => actions.setConfigure);
  const [settings, setSettings] = useState([]);

  useEffect(() => {
    getConfig(process).then((res) => {
      setSettings(res);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <tr>
      <td colspan="6">
        <div className="card">
          <header className="card-header">
            <p className="card-header-title">Configure</p>
            <button className="card-header-icon" aria-label="more options">
              <FontAwesomeIcon
                className="has-text-grey"
                size="lg"
                icon={faTimesCircle}
                onClick={() => setConfigure(process.name)}
              />
            </button>
          </header>
          <div className="card-content">
            {settings.length === 0 ? (
              <p>No config file found</p>
            ) : (
              <JSONBasedConfig settings={settings} process={process} />
            )}
          </div>
        </div>
      </td>
    </tr>
  );
};

export default RecipeConfigure;
