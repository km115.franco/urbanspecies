import { faAngleDown, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import RecipeActions from "./RecipeActions";
import RecipeAdvanced from "./RecipeAdvanced";
import RecipeConfigure from "./RecipeConfigure";
import RecipeDetails from "./RecipeDetails";
import RecipeLogs from "./RecipeLogs";
import RecipeMedia from "./RecipeMedia";
import RecipeInfo from "./RecipeInfo";

const RecipeList = ({ title, processes, hideList, hideConfig, hideMedia }) => {
  const [hidden, setHidden] = useState(hideList);
  return (
    <div className="card m-4">
      <header
        className="card-header pointer"
        onClick={() => setHidden(!hidden)}
      >
        <p className="card-header-title">{title}</p>
        <button className="card-header-icon">
          <FontAwesomeIcon icon={hidden ? faAngleRight : faAngleDown} />
        </button>
      </header>
      <div className={`card-content ${hidden && "is-hidden"}`}>
        <table className="table is-hoverable is-fullwidth is-narrow ">
          <tbody>
            {processes.map((process, index) => (
              <>
                <tr key={index}>
                  <RecipeDetails process={process} />
                  <RecipeActions process={process} />
                  <RecipeAdvanced process={process} hideConfig={hideConfig} />
                </tr>
                {process.logs && <RecipeLogs process={process} />}
                {process.configure && <RecipeConfigure process={process} />}
                {process.media && <RecipeMedia process={process} />}
                {process.info && <RecipeInfo process={process} />}
              </>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default RecipeList;
