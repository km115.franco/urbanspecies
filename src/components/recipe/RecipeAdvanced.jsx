import {
  faFile,
  faCog,
  faPhotoVideo,
  faInfo,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useStoreActions } from "easy-peasy";

const RecipeAdvanced = ({ process, hideConfig }) => {
  const setLogs = useStoreActions((actions) => actions.setLogs);
  const setConfigure = useStoreActions((actions) => actions.setConfigure);
  const setMedia = useStoreActions((actions) => actions.setMedia);
  const setInfo = useStoreActions((actions) => actions.setInfo);
  const { name, logs, configure, media, media_browser, config_file, info } =
    process;

  const handleLogs = () => {
    setLogs(name);
  };

  const handleConfigure = () => {
    setConfigure(name);
  };

  const handleMedia = () => {
    setMedia(name);
  };

  const handleInfo = () => {
    setInfo(name);
  };

  return (
    <td className="has-text-right">
      <button
        className={`button is-rounded is-outlined is-info mt-1 mr-2 ${
          logs && "is-focused"
        }`}
        title={logs ? "Hide recipe logs" : "Show recipe logs"}
        onClick={handleLogs}
      >
        <FontAwesomeIcon icon={faFile} />
      </button>
      {config_file && !hideConfig && (
        <button
          className={`button is-rounded is-outlined is-info mt-1 mr-2 ${
            configure && "is-focused"
          }`}
          title={configure ? "Hide configuration" : "Show configuration"}
          onClick={handleConfigure}
        >
          <FontAwesomeIcon icon={faCog} />
        </button>
      )}
      {media_browser && !hideConfig && (
        <button
          className={`button is-rounded is-outlined is-info mt-1 mr-2 ${
            media && "is-focused"
          }`}
          title={media ? "Hide recipe media" : "Show recipe media"}
          onClick={handleMedia}
        >
          <FontAwesomeIcon icon={faPhotoVideo} />
        </button>
      )}
      <button
        className={`button is-rounded is-outlined is-info mt-1 mr-2 ${
          info && "is-focused"
        }`}
        title={info ? "Hide information" : "Show information"}
        onClick={handleInfo}
      >
        <FontAwesomeIcon icon={faInfo} />
      </button>
    </td>
  );
};

export default RecipeAdvanced;
