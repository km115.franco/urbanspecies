import {
  faPlus,
  faTimesCircle,
  faUpload,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useStoreActions } from "easy-peasy";
import { useEffect, useState } from "react";
import {
  addMedia,
  getProcessMedia,
  host,
  removeProcessFile,
} from "../../js/api";

const RecipeMedia = ({ process }) => {
  const setMedia = useStoreActions((actions) => actions.setMedia);
  const [mediaList, setMediaList] = useState([]);
  const [selectedFile, setSelectedFile] = useState();
  const [instructions, setInstructions] = useState("Choose a file");

  const getMediaPath = (path) => {
    return `${host}${path.split("toolkit/recipes")[1]}/media/`;
  };

  const mediaPath = getMediaPath(process.path);

  const handleUpload = () => {
    const formData = new FormData();
    formData.append("mediaFile", selectedFile, selectedFile.name);
    formData.append("path", process.path);
    addMedia(formData).then(() => {
      const newList = [...mediaList];
      newList.push(selectedFile.name);
      setMediaList(newList);
    });
  };

  const handleFileChange = (e) => {
    setSelectedFile(e.target.files[0]);
  };

  const handleRemove = (file) => {
    removeProcessFile({ path: process.path, file }).then(() => {
      const newList = [...mediaList];
      newList.splice(newList.indexOf(file), 1);
      setMediaList(newList);
    });
  };

  useEffect(() => {
    getProcessMedia(process).then((res) => {
      setMediaList(res);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setInstructions(selectedFile?.name || "Choose a file ...");
  }, [selectedFile]);

  return (
    <tr>
      <td colspan="6">
        <div className="card">
          <header className="card-header">
            <p className="card-header-title">Media</p>
            <button className="card-header-icon" aria-label="more options">
              <FontAwesomeIcon
                className="has-text-grey"
                size="lg"
                icon={faTimesCircle}
                onClick={() => setMedia(process.name)}
              />
            </button>
          </header>
          <div className="card-content">
            <table className="table is-hoverable is-fullwidth is-narrow">
              <tbody>
                {mediaList.map((file, index) => (
                  <tr key={index}>
                    <td>{file}</td>
                    <td>
                      <audio
                        className="mt-2"
                        controls
                        src={`${mediaPath}${file}`}
                      >
                        Your browser does not support the
                        <code>audio</code> element.
                      </audio>
                    </td>
                    <td>
                      <button
                        className="button is-small is-rounded is-danger is-outlined mt-1"
                        onClick={() => handleRemove(file)}
                      >
                        <FontAwesomeIcon icon={faTrash} className="mr-2" />
                        <span>Remove</span>
                      </button>
                    </td>
                  </tr>
                ))}
                <tr>
                  <td colSpan={3} className="is-flex mt-4">
                    <div className="file">
                      <label className="file-label">
                        <input
                          className="file-input"
                          type="file"
                          onChange={handleFileChange}
                        />
                        <span className="file-cta">
                          <FontAwesomeIcon
                            icon={faUpload}
                            className="file-icon"
                          />
                          <span className="file-label">{instructions}</span>
                        </span>
                      </label>
                    </div>
                    <button
                      className="button is-rounded is-success is-outlined ml-4"
                      disabled={!selectedFile?.name}
                      onClick={handleUpload}
                    >
                      <FontAwesomeIcon icon={faPlus} className="mr-2" />
                      <span>Add sound</span>
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </td>
    </tr>
  );
};

export default RecipeMedia;
