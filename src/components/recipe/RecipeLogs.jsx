import { useEffect, useState } from "react";
import { faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { getLogs, socket } from "../../js/api";
import { toDateTime } from "../../js/helpers";
import { useStoreActions } from "easy-peasy";

const RecipeLogs = ({ process }) => {
  const { name } = process;
  const [logs, setLogs] = useState([]);
  const [message, setMessage] = useState("");
  const [type, setType] = useState("");
  const [timestamp, setTimestamp] = useState("");
  const showLogs = useStoreActions((actions) => actions.setLogs);

  useEffect(() => {
    getLogs(process).then((res) => {
      setLogs(
        res.map((log) => ({
          message: log.data,
          timestamp: toDateTime(log.at),
          type: log.type,
        }))
      );
    });

    socket.on("log:out", (msg) => {
      console.log(msg);
      if (name === msg.name) {
        setType(true);
        setTimestamp(toDateTime(msg.at));
        setMessage(msg.data);
      }
    });

    socket.on("log:err", (msg) => {
      console.log(msg);
      if (name === msg.name) {
        setType(false);
        setTimestamp(toDateTime(msg.at));
        setMessage(msg.data);
      }
    });

    return () => {
      socket.off("log:out");
      socket.off("log:err");
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const newLogs = [...logs];
    if (newLogs.length >= 50) {
      newLogs.pop();
    }
    newLogs.unshift({ timestamp, message, type });
    setLogs(newLogs);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [message]);

  return (
    <tr>
      <td colspan="6">
        <div className="card">
          <header className="card-header">
            <p className="card-header-title">Logs</p>

            <button
              className="card-header-icon"
              onClick={() => console.log(logs)}
            >
              <FontAwesomeIcon
                icon={faTimesCircle}
                size="lg"
                className="has-text-grey"
                onClick={() => showLogs(process.name)}
              />
            </button>
          </header>
          <div className="card-content">
            <ul className="is-reverse-scrollable">
              {logs.map((log) => (
                <li>
                  <span className="has-text-grey hide-select">
                    {log.timestamp}
                  </span>
                  <span>{"  "}</span>
                  <span
                    className={
                      log.type ? "has-text-success" : "has-text-danger"
                    }
                  >
                    {log.message}
                  </span>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </td>
    </tr>
  );
};

export default RecipeLogs;
