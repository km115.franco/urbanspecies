import { faTimesCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useStoreActions } from "easy-peasy";
import { useEffect, useState } from "react";
import ReactMarkdown from "react-markdown";
import { getFile, host } from "../../js/api";

const RecipeInfo = ({ process }) => {
  const setInfo = useStoreActions((actions) => actions.setInfo);
  const friendlyPath = process.path.replace("/home/pi", "~");
  const [documentation, setDocumentation] = useState("");
  const getDocPath = (path) => {
    return `${host}${path.split("toolkit/recipes")[1]}`;
  };
  const mediaPath = getDocPath(process.path);

  const addImgPath = (markdown) => {
    return markdown.replaceAll("![img](./", `![img](${mediaPath}/`);
  };

  useEffect(() => {
    getFile(mediaPath, "README.md").then((res) =>
      setDocumentation(addImgPath(res))
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const h1 = (props) => {
    return <h1 className="title is-3">{props.children}</h1>;
  };
  const h2 = (props) => {
    return <h2 className="title is-4">{props.children}</h2>;
  };
  const h3 = (props) => {
    return <h3 className="title is-5">{props.children}</h3>;
  };

  return (
    <tr>
      <td colspan="6">
        <div className="card">
          <header className="card-header">
            <p className="card-header-title">Info</p>
            <p className="card-header-title help has-text-grey">
              {friendlyPath}
            </p>
            <button className="card-header-icon" aria-label="Hide info">
              <FontAwesomeIcon
                className="has-text-grey"
                size="lg"
                icon={faTimesCircle}
                onClick={() => setInfo(process.name)}
              />
            </button>
          </header>
          <div className="card-content is-scrollable">
            <ReactMarkdown components={{ h1, h2, h3 }}>
              {documentation}
            </ReactMarkdown>
          </div>
        </div>
      </td>
    </tr>
  );
};

export default RecipeInfo;
