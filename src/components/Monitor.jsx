import RedisLogs from "./RedisLogs";

const Monitor = () => {
  return (
    <div>
      <div className="card m-4">
        <div className="card-content">
          <h1 className="title is-3 has-text-centered">Redis Monitor</h1>
          <h3 className="subtitle is-5 has-text-centered">
            Here you can see the messages being send by the recipes
          </h3>
          <hr />
          <RedisLogs />
          <hr />
        </div>
      </div>
    </div>
  );
};

export default Monitor;
