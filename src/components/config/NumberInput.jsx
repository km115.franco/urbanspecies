import { useStoreState } from "easy-peasy";
import { getIndexLabel } from "../../js/helpers";

const NumberInput = ({ prop, handleChange }) => {
  const recipeSettings = useStoreState((state) => state.recipeSettings);

  const handleInput = (e) => {
    let value = e.target.value;
    if (prop.type === "int") value = parseInt(value);
    if (prop.type === "float") value = parseFloat(value);
    handleChange(value, prop);
  };

  return (
    <div className="field">
      <label className={`label ${"index" in prop && "has-text-grey is-1"}`}>
        {getIndexLabel(recipeSettings, prop.label, prop.index)}
      </label>
      <div className="control">
        <input
          className="input"
          type="number"
          placeholder={prop.placeholder}
          value={prop.value}
          onChange={handleInput}
          max={prop.validators?.max}
          min={prop.validators?.min}
          readOnly={prop.readOnly}
        />
      </div>
    </div>
  );
};
export default NumberInput;
