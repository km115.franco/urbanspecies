import { faPlus, faSave, faTrash } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useStoreState } from "easy-peasy";
import { useEffect, useState } from "react";
import MultipleSelect from "./MultipleSelect";
import NumberInput from "./NumberInput";
import SelectInput from "./SelectInput";
import TextInput from "./TextInput";

const ArrayInput = ({ prop, handleChange }) => {
  const recipeSettings = useStoreState((state) => state.recipeSettings);
  const [items, setItems] = useState(prop.value);
  const [components, setComponents] = useState([]);
  const [saved, setSaved] = useState(true);

  const valueOrReference = (reference) => {
    if (reference) {
      return typeof reference === "number"
        ? reference
        : recipeSettings.find((x) => x.property === reference).value;
    }
    return null;
  };

  const constrain = valueOrReference(prop.length);

  const handleErase = (i) => {
    const newItems = [...items];
    newItems.splice(i, 1);
    setItems(newItems);
  };

  const handleNew = () => {
    const newItems = [...items];
    newItems.push("");
    setItems(newItems);
  };

  const handleSave = () => {
    handleChange(items, prop);
    setSaved(true);
  };

  const handleLocalChange = (value, prop) => {
    const { index } = prop;

    setComponents(
      components.map((p) => {
        return p.index === index ? { ...p, value } : p;
      })
    );
    setItems(items.map((x, i) => (i === index ? value : x)));
  };

  useEffect(() => {
    const createComponents = (items) => {
      let objects = [];
      items.forEach((value, index) => {
        objects.push({ ...prop.element, value, index });
      });
      return objects;
    };
    setComponents(createComponents(items));
    setSaved(false);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [items]);

  useEffect(() => {
    if (constrain) {
      const newItems = [...items];
      if (items.length < constrain) {
        while (newItems.length < constrain) {
          newItems.push("");
        }
      }
      if (items.length > constrain) {
        newItems.splice(constrain);
      }
      setItems(newItems);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [constrain]);

  return (
    <div className="field">
      <label className="label">{prop.label}</label>
      {components.map((component, i) => (
        <div className="ml-4 is-flex">
          {component.type === "text" && (
            <TextInput prop={component} handleChange={handleLocalChange} />
          )}
          {(component.type === "int" || component.type === "float") && (
            <NumberInput prop={component} handleChange={handleLocalChange} />
          )}
          {component.type === "select" && (
            <SelectInput prop={component} handleChange={handleLocalChange} />
          )}
          {component.type === "multiple-select" && (
            <MultipleSelect prop={component} handleChange={handleLocalChange} />
          )}

          {!constrain && (
            <button
              className="mt ml-2 button is-small is-danger is-rounded is-outlined"
              onClick={() => handleErase(i)}
            >
              <FontAwesomeIcon icon={faTrash} />
              <span className="ml-2">Remove</span>
            </button>
          )}
        </div>
      ))}
      {!constrain && (
        <button
          className="ml-4 button is-success is-outlined is-small is-rounded"
          onClick={handleNew}
        >
          <FontAwesomeIcon icon={faPlus} />
          <span className="ml-2">Add</span>
        </button>
      )}

      <br />
      <button
        className={`ml-4 mt-2 button is-outlined is-small is-rounded ${
          saved ? "is-success" : "is-link"
        }`}
        onClick={handleSave}
      >
        <FontAwesomeIcon icon={faSave} />
        <span className="ml-2">Save</span>
      </button>
    </div>
  );
};
export default ArrayInput;
