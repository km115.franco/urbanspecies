import MultipleSelect from "./MultipleSelect";

const ObjectEdit = ({ prop, handleChange }) => {
  return (
    <div className="field">
      <label className="label">{prop.label}</label>
      <>
        {prop.keys.type === "multiple-select" && (
          <MultipleSelect prop={prop.keys} handleChange={handleChange} />
        )}
      </>
    </div>
  );
};
export default ObjectEdit;
