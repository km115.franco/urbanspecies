import { useStoreState } from "easy-peasy";
import { useEffect, useState } from "react";
import { getProcessMedia } from "../../js/api";

const MultipleSelect = ({ prop, handleChange }) => {
  const [selected, setSelected] = useState(prop.value);
  const [options, setOptions] = useState(prop.options);
  const processes = useStoreState((state) => state.processes);
  const process = processes.find((x) => x.configure);

  const handleClick = (option) => {
    console.log(option);
    const tempSelected = [...selected];
    tempSelected.includes(option)
      ? tempSelected.splice(tempSelected.indexOf(option), 1)
      : tempSelected.push(option);
    setSelected(tempSelected);
    handleChange(tempSelected, prop);
  };

  useEffect(() => {
    if (prop.source === "media") {
      getProcessMedia(process).then((res) => setOptions(res));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="field">
      <label className={`label ${"index" in prop && "has-text-grey is-1"}`}>
        {prop.label}
      </label>
      {options.map((o) => (
        <button
          className={`button ml-4 mb-4 is-rounded ${
            selected.includes(o) ? "is-success" : "is-warning"
          }`}
          onClick={() => handleClick(o)}
        >
          {o}
        </button>
      ))}
    </div>
  );
};
export default MultipleSelect;
