import { useStoreState } from "easy-peasy";
import { getIndexLabel } from "../../js/helpers";

const TextInput = ({ prop, handleChange }) => {
  const recipeSettings = useStoreState((state) => state.recipeSettings);

  const handleInput = (e) => {
    const text = e.target.value;
    handleChange(text, prop);
  };

  return (
    <div className="field">
      <label className={`label ${"index" in prop && "has-text-grey is-1"}`}>
        {getIndexLabel(recipeSettings, prop.label, prop.index)}
      </label>
      <div className="control">
        <input
          className="input"
          type="text"
          placeholder={prop.placeholder}
          value={prop.value}
          onChange={handleInput}
          maxLength={prop.validators?.maxLength}
          minLength={prop.validators?.maxLength}
        />
      </div>
    </div>
  );
};
export default TextInput;
