import { useStoreState } from "easy-peasy";
import { useEffect, useState } from "react";
import { getProcessMedia } from "../../js/api";
import { getIndexLabel } from "../../js/helpers";

const SelectInput = ({ prop, handleChange }) => {
  const recipeSettings = useStoreState((state) => state.recipeSettings);
  const processes = useStoreState((state) => state.processes);
  const [options, setOptions] = useState(prop.options);
  const process = processes.find((x) => x.configure);

  const handleSelect = (e) => {
    handleChange(e.target.value, prop);
  };

  useEffect(() => {
    if (prop.source === "media") {
      getProcessMedia(process).then((res) => setOptions(res));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="field">
      <label className={`label ${"index" in prop && "has-text-grey is-1"}`}>
        {getIndexLabel(recipeSettings, prop.label, prop.index)}
      </label>
      <div className="select">
        <select onClick={handleSelect}>
          <option value="" selected={"" === prop.value} disabled hidden>
            Choose here
          </option>
          {options.map((o) => (
            // eslint-disable-next-line
            <option selected={o == prop.value} value={o}>
              {o}
            </option>
          ))}
        </select>
      </div>
    </div>
  );
};
export default SelectInput;
