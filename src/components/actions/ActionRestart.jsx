import { faRedo } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { restartProcess } from "../../js/api";

const ActionRestart = ({ process }) => {
  return (
    <td>
      <button
        className="button is-rounded is-outlined mt-1 is-danger"
        onClick={() => restartProcess(process)}
      >
        <FontAwesomeIcon icon={faRedo} />
        <span className="ml-2">Restart</span>
      </button>
    </td>
  );
};

export default ActionRestart;
