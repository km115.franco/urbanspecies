import { faBan, faPowerOff } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useStoreActions, useStoreState } from "easy-peasy";
import { saveDaemons } from "../../js/api";

const ActionSetDaemon = ({ process }) => {
  const setDaemon = useStoreActions((actions) => actions.setDaemon);
  const processes = useStoreState((state) => state.processes);

  const handleAutoStart = () => {
    setDaemon(process.name);
    let previousDaemons = processes.filter((p) => p.daemon && !p.core);
    if (!process.daemon) previousDaemons.push(process);
    else {
      previousDaemons = previousDaemons.filter((p) => p.name !== process.name);
    }

    saveDaemons(previousDaemons);
  };

  return (
    <td>
      {process.daemon ? (
        <button
          className="button is-rounded is-outlined mt-1 is-danger"
          onClick={handleAutoStart}
        >
          <FontAwesomeIcon icon={faBan} />
          <span className="ml-2">No Autostart</span>
        </button>
      ) : (
        <button
          className="button is-rounded is-outlined mt-1 is-success"
          onClick={handleAutoStart}
        >
          <FontAwesomeIcon icon={faPowerOff} />
          <span className="ml-2">Autostart</span>
        </button>
      )}
    </td>
  );
};

export default ActionSetDaemon;
