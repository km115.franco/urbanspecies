import { faStop, faPlay } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useStoreActions } from "easy-peasy";
import { startProcess, stopProcess } from "../../js/api";

const ActionSetRun = ({ process }) => {
  const setRunning = useStoreActions((actions) => actions.setRunning);

  const handleStartButton = () => {
    startProcess(process).then(
      (res) => res.msg && setRunning({ name: process.name, running: true })
    );
  };

  const handleStopButton = () => {
    stopProcess(process).then(
      (res) => res.msg && setRunning({ name: process.name, running: false })
    );
  };

  return (
    <td>
      {process.running ? (
        <button
          className="button is-rounded is-outlined mt-1 is-danger"
          onClick={handleStopButton}
        >
          <FontAwesomeIcon icon={faStop} />
          <span className="ml-2">Stop</span>
        </button>
      ) : (
        <button
          className="button is-rounded is-outlined mt-1 is-success"
          onClick={handleStartButton}
        >
          <FontAwesomeIcon icon={faPlay} />
          <span className="ml-2">Start</span>
        </button>
      )}
    </td>
  );
};

export default ActionSetRun;
