import { faPlusCircle, faMinusCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useStoreActions, useStoreState } from "easy-peasy";
import { copyToMyRecipes, removeFromMyRecipes } from "../../js/api";

const ActionSetExample = ({ process }) => {
  const processes = useStoreState((state) => state.processes);
  const addProcess = useStoreActions((actions) => actions.addProcess);
  const removeProcess = useStoreActions((actions) => actions.removeProcess);
  const isSaved = processes
    .map((x) => x.name)
    .includes(process.name.replace(/\*$/, ""));

  const handleRemoveRecipe = () => {
    if (window.confirm("Do you really want to remove this recipe?")) {
      removeFromMyRecipes(process);
      removeProcess(process);
    }
  };

  const handleCopyRecipe = () => {
    copyToMyRecipes(process);
    const newProcess = {
      ...process,
      name: process.name.replace(/\*$/, ""),
      path: process.path.replace("/examples/", "/myRecipes/"),
      running: false,
      example: false,
    };
    addProcess(newProcess);
  };

  return (
    <td>
      {process.example ? (
        <button
          className="button is-rounded is-outlined mt-1 is-success"
          onClick={handleCopyRecipe}
          disabled={isSaved}
        >
          <FontAwesomeIcon icon={faPlusCircle} />
          <span className="ml-2">Add to </span>
        </button>
      ) : (
        <button
          className="button is-rounded is-outlined mt-1 is-danger"
          onClick={handleRemoveRecipe}
        >
          <FontAwesomeIcon icon={faMinusCircle} />
          <span className="ml-2">Remove</span>
        </button>
      )}
    </td>
  );
};

export default ActionSetExample;
