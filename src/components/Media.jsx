import { faPlus, faTrash, faUpload } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import { addMedia, getMedia, host, removeMedia } from "../js/api";

const Media = () => {
  const [mediaList, setMediaList] = useState([]);
  const [instructions, setInstructions] = useState("Choose a file ...");
  const [selectedFile, setSelectedFile] = useState();

  const handleUpload = () => {
    const formData = new FormData();
    formData.append("mediaFile", selectedFile, selectedFile.name);
    addMedia(formData).then(() => {
      const newList = [...mediaList];
      newList.push(selectedFile.name);
      setMediaList(newList);
    });
  };

  const handleRemove = (file) => {
    removeMedia({ file }).then(() => {
      const newList = [...mediaList];
      newList.splice(newList.indexOf(file), 1);
      setMediaList(newList);
    });
  };

  const handleFileChange = (e) => {
    setSelectedFile(e.target.files[0]);
  };

  useEffect(() => {
    getMedia().then((res) => {
      setMediaList(res);
    });
  }, []);

  useEffect(() => {
    setInstructions(selectedFile?.name || "Choose a file ...");
  }, [selectedFile]);

  return (
    <div className="card m-4">
      <div className="card-content">
        <h1 className="title is-4">Media</h1>
        <table className="table is-hoverable is-fullwidth is-narrow">
          <tbody>
            {mediaList.map((file, index) => (
              <tr key={index}>
                <td>{file}</td>
                <td>
                  <audio controls src={`${host}/media/${file}`}>
                    Your browser does not support the
                    <code>audio</code> element.
                  </audio>
                </td>
                <td>
                  <button
                    className="button is-small is-rounded is-danger is-outlined"
                    onClick={() => handleRemove(file)}
                  >
                    <FontAwesomeIcon icon={faTrash} className="mr-2" />
                    <span>Remove</span>
                  </button>
                </td>
              </tr>
            ))}
            <tr>
              <td colSpan={3} className="is-flex mt-4">
                <div className="file">
                  <label className="file-label">
                    <input
                      className="file-input"
                      type="file"
                      onChange={handleFileChange}
                    />
                    <span className="file-cta">
                      <FontAwesomeIcon icon={faUpload} className="file-icon" />
                      <span className="file-label">{instructions}</span>
                    </span>
                  </label>
                </div>
                <button
                  className="button is-rounded is-success is-outlined ml-4"
                  disabled={!selectedFile?.name}
                  onClick={handleUpload}
                >
                  <FontAwesomeIcon icon={faPlus} className="mr-2" />
                  <span>Add sound</span>
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Media;
