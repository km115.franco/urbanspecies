import { useEffect, useState } from "react";
import { getLogs, sendToRedis, socket } from "../js/api";
import { toDateTime } from "../js/helpers";

const RedisLogs = () => {
  const name = "Redis Monitor";
  const [logs, setLogs] = useState([]);
  const [message, setMessage] = useState("");
  const [type, setType] = useState("");
  const [timestamp, setTimestamp] = useState("");
  const [filter, setFilter] = useState("");
  const [channel, setChannel] = useState("fromMesh");
  const [payload, setPayload] = useState("");

  const handleFilter = (e) => {
    setFilter(e.target.value);
  };

  const handleSend = () => {
    sendToRedis({ channel, payload });
  };

  useEffect(() => {
    getLogs({ name }).then((res) => {
      setLogs(
        res.map((log) => ({
          message: log.data,
          timestamp: toDateTime(log.at),
          type: log.type,
        }))
      );
    });

    socket.on("log:out", (msg) => {
      console.log(msg);
      if (name === msg.name) {
        setType(true);
        setTimestamp(toDateTime(msg.at));
        setMessage(msg.data);
      }
    });

    socket.on("log:err", (msg) => {
      console.log(msg);
      if (name === msg.name) {
        setType(false);
        setTimestamp(toDateTime(msg.at));
        setMessage(msg.data);
      }
    });

    return () => {
      socket.off("log:out");
      socket.off("log:err");
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const newLogs = [...logs];
    if (newLogs.length >= 500) {
      newLogs.pop();
    }
    if (filter.length > 1) {
      if (message.includes(filter))
        newLogs.unshift({ timestamp, message, type });
    } else {
      newLogs.unshift({ timestamp, message, type });
    }

    setLogs(newLogs);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [timestamp]);

  return (
    <div className="card">
      <header className="card-header">
        <div className="is-flex m-4 is-justify-content-space-around container">
          <div class="field">
            <label class="label">Send message:</label>
            <div class="control  m-1">
              <input
                class="input"
                type="text"
                placeholder="Channel"
                onChange={(e) => setChannel(e.target.value)}
                value={channel}
              />
            </div>
            <div class="control  m-1">
              <input
                class="input"
                type="text"
                placeholder="Payload"
                onChange={(e) => setPayload(e.target.value)}
                value={payload}
              />
            </div>
            <button
              onClick={handleSend}
              className="button m-1 is-success is-outlined"
            >
              Send
            </button>
          </div>
          <div class="field">
            <label class="label">Filter</label>
            <div class="control">
              <input
                onChange={handleFilter}
                value={filter}
                class="input"
                type="text"
                placeholder=""
              />
            </div>
          </div>
        </div>
      </header>
      <header className="card-header">
        <p className="card-header-title">Logs</p>
      </header>
      <div className="card-content">
        <ul className="is-reverse-scrollable">
          {logs.map((log) => (
            <tr
              style={{
                border: "1px solid rgb(220, 220, 220)",
                padding: "0 0.25em",
              }}
            >
              <td
                className="has-text-grey hide-select"
                style={{ width: "10em" }}
              >
                {log.timestamp}
              </td>
              <td className={log.type ? "has-text-success" : "has-text-danger"}>
                {log.message}
              </td>
            </tr>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default RedisLogs;
