import { Route, Switch } from "react-router";
import { StoreProvider, createStore } from "easy-peasy";

import Documentation from "./Documentation";
import Firefly from "./Firefly";
import Home from "./Home";
import Navbar from "./Navbar";
import Recipes from "./Recipes";
import Settings from "./Settings";
import model from "../js/model";
import Monitor from "./Monitor";

const store = createStore(model);

const App = () => {
  return (
    <StoreProvider store={store}>
      <Navbar />
      <div className="container">
        <Switch>
          <Route path="/home" component={Home} />
          <Route path="/recipes" component={Recipes} />
          <Route path="/firefly" component={Firefly} />
          <Route path="/monitor" component={Monitor} />
          <Route path="/documentation" component={Documentation} />
          <Route path="/settings" component={Settings} />
          <Route path="/" component={Home} />
        </Switch>
      </div>
    </StoreProvider>
  );
};

export default App;
