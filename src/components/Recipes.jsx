import { useStoreActions, useStoreState } from "easy-peasy";
import { useEffect } from "react";
import { getDaemons, getRecipes, listProcesses } from "../js/api";
import RecipeList from "./recipe/RecipeList";
import { socket } from "../js/api";

const Recipes = () => {
  const processes = useStoreState((state) => state.processes);
  const setProcesses = useStoreActions((actions) => actions.setProcesses);
  const setRunning = useStoreActions((actions) => actions.setRunning);
  const setDaemon = useStoreActions((actions) => actions.setDaemon);

  useEffect(() => {
    const getRecipeStatus = (newProcesses) => {
      listProcesses().then((res) => {
        const pm2Processes = res.msg;
        const pm2Names = pm2Processes.map((p) => p.name);
        const pm2Status = pm2Processes.map((p) => p.status === "online");

        newProcesses.forEach((proc) => {
          const { name } = proc;
          if (pm2Names.includes(name)) {
            setRunning({ name, running: pm2Status[pm2Names.indexOf(name)] });
          }
        });

        getDaemons().then((res) => {
          const daemonNames = res.filter((r) => !r.core).map((r) => r.name);
          console.log("Daemon names:", daemonNames);
          newProcesses.forEach((proc) => {
            const { name } = proc;
            if (daemonNames.includes(name)) setDaemon(name);
          });
        });
      });
    };

    getRecipes().then((res) => {
      setProcesses(res);
      getRecipeStatus(res);
    });

    socket.on("process:event", (msg) => {
      const { name, status } = msg;
      setRunning({ name, running: status === "online" });
    });

    return () => {
      socket.off("process:event");
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <RecipeList
        title="Core"
        core={true}
        processes={processes.filter(
          (p) => p.core && p.name !== "Redis Monitor"
        )}
        hideList
      />
      <RecipeList
        title={"My recipes"}
        core={false}
        processes={processes.filter((p) => !p.core && !p.example)}
      />
      <RecipeList
        title={"Community examples"}
        core={false}
        processes={processes.filter((p) => p.example)}
        hideList
        hideMedia
      />
    </div>
  );
};

export default Recipes;
