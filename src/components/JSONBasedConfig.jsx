import { useStoreActions, useStoreState } from "easy-peasy";
import { useEffect, useState } from "react";
import { restartProcess, setConfig } from "../js/api";
import ArrayInput from "./config/ArrayInput";
import MultipleSelect from "./config/MultipleSelect";
import NumberInput from "./config/NumberInput";
import SelectInput from "./config/SelectInput";
import TextInput from "./config/TextInput";

const JSONBasedConfig = ({ settings, process }) => {
  const recipeSettings = useStoreState((state) => state.recipeSettings);
  const setRecipeSettings = useStoreActions(
    (actions) => actions.setRecipeSettings
  );
  const [saved, setSaved] = useState(true);

  const handleSave = () => {
    setConfig(process, recipeSettings).then((res) => {
      console.log("Success saving");
    });
    setSaved(true);
    restartProcess(process);
  };

  const handleDownload = () => {
    const getFriendlyValues = (settings) => {
      const lines = settings.map((x) => `${x.label}: ${x.value}`);
      let text = `${process.name} Configuration\n\r`;
      lines.forEach((line) => {
        text = text + line + "\n\r";
      });
      return text;
    };
    let currentTime = new Date();
    let currentStr = currentTime.toLocaleString("sv-SE").split(/-|:/).join("");
    let filePrefix = currentStr.replace(" ", "_");
    let dataStr =
      "data:text/json;charset=utf-8," +
      encodeURIComponent(getFriendlyValues(recipeSettings));
    let dlAnchorElem = document.querySelector("#download");
    dlAnchorElem.setAttribute("href", dataStr);
    dlAnchorElem.setAttribute("download", `${filePrefix}_${process.name}.txt`);
  };

  const handleChange = (value, prop) => {
    const { property } = prop;

    setRecipeSettings(
      recipeSettings.map((p) => {
        return p.property === property ? { ...p, value } : p;
      })
    );
  };

  useEffect(() => {
    setSaved(recipeSettings === settings);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [recipeSettings]);

  useEffect(() => {
    setRecipeSettings(settings);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [settings]);

  return (
    <div>
      {recipeSettings.map((p) => (
        <>
          {p.type === "text" && (
            <TextInput prop={p} handleChange={handleChange} />
          )}
          {(p.type === "int" || p.type === "float") && (
            <NumberInput prop={p} handleChange={handleChange} />
          )}
          {p.type === "select" && (
            <SelectInput prop={p} handleChange={handleChange} />
          )}
          {p.type === "multiple-select" && (
            <MultipleSelect prop={p} handleChange={handleChange} />
          )}
          {p.type === "array" && (
            <ArrayInput prop={p} handleChange={handleChange} />
          )}
        </>
      ))}
      <div className="is-flex is-align-items-center pt-4">
        <button
          className={`button is-rounded is-outlined ${
            saved ? "is-success" : "is-link"
          }`}
          onClick={handleSave}
        >
          {"Save"}
        </button>
        <a
          className={`ml-4 ${saved ? "is-success" : "is-link"}`}
          onClick={handleDownload}
          id="download"
          href="Download configuration"
        >
          {"Download config"}
        </a>
      </div>

      {!saved && (
        <p className="help is-grey">Changes found, save before leave</p>
      )}
    </div>
  );
};

export default JSONBasedConfig;
