import { useState } from "react";
import { Link } from "react-router-dom";
import logo from "../assets/logo.svg";

const Navbar = () => {
  const [showMenu, setShowMenu] = useState(false);

  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="navbar-brand is-align-items-center">
        <Link to="/home">
          <img alt="logo" src={logo} className="logo m-4" />
        </Link>
        <a
          role="button"
          className={`navbar-burger ${showMenu && "is-active"}`}
          ariaLabel="menu"
          ariaExpanded="false"
          dataTarget="navbarBasicExample"
          onClick={() => setShowMenu(!showMenu)}
        >
          <span ariaHidden="true"></span>
          <span ariaHidden="true"></span>
          <span ariaHidden="true"></span>
        </a>
      </div>

      <div
        className={`navbar-menu ${showMenu && "is-active"}`}
        id="navbarBasicExample"
      >
        <div className="navbar-start ml-4 has-text-weight-medium">
          <Link className="navbar-item" to="/home">
            HOME
          </Link>
          <Link className="navbar-item" to="/recipes">
            RECIPES
          </Link>
          <Link className="navbar-item" to="/firefly">
            FIREFLY
          </Link>
          <Link className="navbar-item" to="/monitor">
            MONITOR
          </Link>
          <Link className="navbar-item" to="/documentation">
            DOCUMENTATION
          </Link>
          <Link className="navbar-item" to="/settings">
            SETTINGS
          </Link>
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
