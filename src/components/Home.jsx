import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import {
  getCurrentVersion,
  getLatestVersion,
  restartHost,
  shutdownHost,
  testHost,
  updateFromRepository,
} from "../js/api";

const Home = () => {
  const backTime = 15;
  const [time, setTime] = useState();
  const [seconds, setSeconds] = useState(backTime);
  const [showModal, setShowModal] = useState(false);
  const [action, setAction] = useState();
  const [newRelease, setNewRelease] = useState(false);
  const [updating, setUpdating] = useState(false);
  const [currentVersion, setCurrentVersion] = useState("0.0.0");
  const [latestVersion, setLatestVersion] = useState("0.0.0");

  const handleShutdown = () => {
    if (window.confirm("Do you really want to shutdown?")) {
      shutdownHost().then(() => setShowModal(true));
      setAction("shutdown");
    }
  };

  const handleRestart = () => {
    if (window.confirm("Do you really want to restart?")) {
      restartHost().then(() => setShowModal(true));
      setAction("reboot");
    }
  };

  const handleReconnect = () => {
    testHost()
      .then(() => setShowModal(false))
      .catch(setSeconds(backTime));
  };

  const handleUpdate = () => {
    setUpdating(true);
    updateFromRepository().then((res) => {
      setUpdating(false);
      setTimeout(() => {
        window.location.reload();
      }, 5000);
    });
  };

  useEffect(() => {
    const interval = setInterval(() => {
      setTime(Date.now());
    }, 1000);

    getCurrentVersion().then((current) => {
      setCurrentVersion(current);
      getLatestVersion().then((latest) => {
        setLatestVersion(latest);
        if (current !== latest) {
          setNewRelease(true);
        } else {
          setNewRelease(false);
        }
      });
    });

    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    if (seconds > 0) {
      if (showModal) setSeconds(seconds - 1);
      else setSeconds(backTime);
    } else {
      handleReconnect();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [time]);

  return (
    <div>
      <div className="card m-4">
        <div className="card-content">
          <h1 className="title is-3 has-text-centered">Welcome</h1>
          <h3 className="subtitle is-5 has-text-centered">
            Welcome to the Urban Species Toolkit
          </h3>
          <hr />
          <p className="header has-text-centered">Version: {currentVersion} </p>
          <hr />
          <div className="is-flex is-flex-centered">
            {newRelease ? (
              <>
                <p>New version available!</p>
                <button
                  className={`button ml-4 is-rounded is-outlined is-success ${
                    updating && "is-loading"
                  }`}
                  onClick={handleUpdate}
                >
                  Update to version {latestVersion}
                </button>
              </>
            ) : (
              <p className="has-text-grey">No updates available</p>
            )}
          </div>
        </div>
        <div className="card-footer">
          <button
            onClick={handleShutdown}
            className="card-footer-item button is-info m-4"
          >
            Shutdown
          </button>
          <button
            onClick={handleRestart}
            className="card-footer-item button is-info m-4"
          >
            Reboot
          </button>
        </div>
      </div>
      <div className={`modal ${showModal && "is-active"}`}>
        <div className="modal-background"></div>
        <div className="modal-content is-flex is-align-items-center is-flex-direction-column">
          <FontAwesomeIcon
            icon={faSpinner}
            spin
            className="has-text-primary is-block"
            size="2x"
          />
          <p className="help is-primary mt-4">
            reconnecting in {seconds} seconds
          </p>
          {action === "shutdown" && (
            <p className="help is-warning mt-4">
              You may have to disconnect and connect the power supply.
            </p>
          )}
          <button
            className="button is-outlined is-success m-4"
            onClick={handleReconnect}
          >
            Reconnect now!
          </button>
        </div>
      </div>
    </div>
  );
};

export default Home;
